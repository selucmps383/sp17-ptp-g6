// Driver File
package babylinkedlist;
import java.util.Scanner;


/**
 *
 * @author christopherdecker
 */
public class Driver {
    public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		BabyLinkedList list = new BabyLinkedList();    // create the linked list
		// prompt user for number of node he/she wants to add
		System.out.print("How many nodes whould u like to add: ");
		int num = sc.nextInt();
		
		// use a for loop to add nodes to the linked list
		for (int i = 0; i < num; i++)
		{
			list.addNodeAtEndOfList(i);
		}
		System.out.println("Content of the list:");
		list.showList();
		
		sc.close();   // close the scanner
	}
}

    
