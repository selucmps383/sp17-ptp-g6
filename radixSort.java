// Java File Sorts the names in the names.txt file using a radix sort using the following: 
// Linked lists used for the buckets
// Sort on the fist 4 letters of the name 
// Count the number of steps/comparisons
// After each pass, write out the buckets so that we can see the progression 
// of the sort.
package radixsort;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Vector;


public class RadixSort {
	
	/**
	 * read names from file.
	 * @return
	 * a vector stores all the names.
	 */
    
        @SuppressWarnings("CallToPrintStackTrace")
	public static Vector<String> readNames()
	{
		Vector<String> names = new Vector<>();
		File f = new File("names.txt");
		try {
                    // Read a line from file
                    try (Scanner sc = new Scanner(f)) {
                        // Read a line from file
                        while (sc.hasNext())
                        {
                            String line = sc.nextLine();
                            // Add the names to the vector
                            names.add(line);
                        }
                    }
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return names;
	}
	
	/**
	 * Sort the names using radix sort.
	 * @param names
	 * A vector contains names.
	 */
        
	public static void radixSort(Vector<String> names)
	{
		int steps = 0;
		// Use linked lists for buckets
		LinkedList<String>[] list = new LinkedList[27];
		
		// Initialize the buckets
		for (int i = 0; i < 27; i++)
		{
			list[i] = new LinkedList<>();
		}
		
		// Sort on the fist 4 letters of the name
		for (int i = 3; i >= 0; i--)
		{
			for (String name : names)
			{
				steps++;
				if (name.length() <= i)
				{
					list[0].add(name);
				}
				else
				{
					int index = (int)(name.charAt(i) - 'a') + 1;
					list[index].add(name);
				}
			}
			
			// Add the sorted names back to the vector
			// and print out the buckets in the meantime
			names.clear();
			for (int j = 0; j < 27; j++)
			{
				while (!list[j].isEmpty())
				{
					String name = (String) list[j].poll();
					names.add(name);
					// Print out the names
					System.out.print(name + "  ");
				}
			}
			System.out.println();
		}
		// Print out the steps
		System.out.println("Total steps: " + steps);
	}
	
	
	public static void main(String[] args)
	{
		// Read names from the file
		Vector<String> names = readNames();
		// Sort the names using radix sort
		radixSort(names);
	}
}
