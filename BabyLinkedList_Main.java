package babylinkedlist;

/**
 *
 * @author christopherdecker 
 */
public class BabyLinkedList {

    	private class Node{
		private int data;
		private Node next;
		
		public Node(int data)
		{
			this.data = data;
			this.next = null;
		}
	}
	
	private Node head;
	private int size;
	
	/**
	 * default constructor
	 */
	public BabyLinkedList()
	{
		this.head = null;
		this.size = 0;
	}
	
	/**
	 * create a new node
	 * @param data
	 * 	Integer that will stored in the newly created node
	 * @return
	 * 	newly create node
	 */
	private Node makeNode(int data)
	{
		return new Node(data);
	}
	
	/**
	 * get the tail node of the list
	 * @return
	 * 	the tail node of the list
	 */
	private Node findTail()
	{
		//return null if the list is empty
		if (head == null)
			return null;
		
		Node tmp = head;
		//go to end of the list
		while (tmp.next != null)
			tmp = tmp.next;
		
		return tmp;
	}
	
	/**
	 * add a node at the end of the list
	 * @param data
	 * 	integer that will be stored in the newly add node
	 */
	public void addNodeAtEndOfList(int data)
	{
		Node newNode = makeNode(data);
		if (head == null)    // list is empty, add to the head
		{
			head = newNode;
			return;
		}
		
		//add to the end of the list
		Node tail = findTail();
		tail.next = newNode;
	}
	
	/**
	 * print the contents of the list to the screen
	 */
	public void showList()
	{
		if (head == null)
		{
			System.out.println("The list is empty");
			return;
		}
		
		Node tmp = head;
		
		while (tmp != null)
		{
			System.out.println(tmp.data);
			tmp = tmp.next;
		}
	}
}
   
