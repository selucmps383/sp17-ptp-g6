﻿#** Git Tutorial **#

## **Distributed Version Control** ##
+ Everyone has their own repository and can share with other members.

+ You can work offline, being online is necessary for sharing changes.

+ With each developer having their own branch, merging is easy.

> ![distributed.png](https://bitbucket.org/repo/BLzrb6/images/3677307195-distributed.png)

+ With Distributed, each developer has a copy of the entire repository. All revision history, all branches, etc.

+ Less network transfers, work locally easier.


## ** Git Extensions **
![gitEXT.png](https://bitbucket.org/repo/BLzrb6/images/1569437822-gitEXT.png)
> Git Extensions Interface

**Git Extensions Commands**

+ To begin with, open Git extensions and click on `Open Repository`, then select the appropriate directory and open it.  

> ![git_ex_start.png](https://bitbucket.org/repo/BLzrb6/images/693604540-git_ex_start.png)  

> ![gitextinterface.png](https://bitbucket.org/repo/BLzrb6/images/235942252-gitextinterface.png)

## **Branching**
+ Gits Branching model is the biggest feature that makes Git stand apart from nearly every other SCM (Software Configuration Management). 

+ Git allows you to have multiple local branches, which can be entirely independent of each other. It literally only takes seconds to create, merge, or delete lines of development.

## ** Creating a Branch on BitBucket using GUI Interface: ** ##

1) Hover your mouse over the left menu 

2) Click Create branch in the left panel.

> ![git-branch.png](https://bitbucket.org/repo/BLzrb6/images/227987025-git-branch.png)

3) From the popup that appears, enter a Branch name and click Create.

> ![git-popup-1.png](https://bitbucket.org/repo/BLzrb6/images/1287314180-git-popup-1.png)

4) Another popup will appear to create your branch

+ There is a drop down menu to select where you want to branch from

> ![Git-popup-2.png](https://bitbucket.org/repo/BLzrb6/images/1563938134-Git-popup-2.png)

+ Next, enter the name for your new branch

> ![Git-popup-3.png](https://bitbucket.org/repo/BLzrb6/images/1880408060-Git-popup-3.png)

+ Now click create

+ After you create a branch, you can check it out from your local system by using the fetch and checkout commands that Bitbucket provides, similar to the following: 

     $ git fetch && git checkout <feature_name>

5) Hover over the menu on the left, and view the Source page of your repository in Bitbucket. You should see both the master and the feature branch. When you select the feature branch, you see the Source page from that perspective. Select the feature branch to view its Recent commits.

> ![Source.png](https://bitbucket.org/repo/BLzrb6/images/1645220288-Source.png)

> ![Source-2.png](https://bitbucket.org/repo/BLzrb6/images/3888290692-Source-2.png)


## **Pull Requests** ## 

+ Allows you to discuss proposed changes before actually integrating them into your official project.

> ![pull2.png](https://bitbucket.org/repo/BLzrb6/images/2492010101-pull2.png)

+ Click the `Create pull request` link.

> ![Source.png](https://bitbucket.org/repo/BLzrb6/images/2178216600-Source.png)

+ Once clicked, you will be redirected to the create page where you can type your message about what changes you would like made.

+ The `Source` is the name of the branch you want to merge.

+ The `Destination` is the destination repository and branch you are merging your changes to. 

+ Whenever you create a pull request, everyone in your group can then see what changes you have made and make comments. 

+ It can also be `edited` and `declined`.

> ![Pullrequestfinal.png](https://bitbucket.org/repo/BLzrb6/images/2479543903-Pullrequestfinal.png)

+ Once everyone is in agreeance, the request can be accepted and merged into the official project.

## ** Merging on BitBucket: ** ##

+ BitBucket has multiple options for merging, and a full reference can be found at https://git-scm.com/docs/git-merge.

+ Merging --> The easiest way to describe Merging is when you want to join two or more development histories together. 

+ Since merging can be fairly detailed with several options, I'm only going to cover the basics of merging.

+ First, as you are working on a project, there will be times when you and at least one other person might be editing the same page. Instead of wondering how you will know if there is someone else making an edit on the same page as you, Git uses a feature called Confluence, that will display a message indicating who else is making an edit. 

>![edit-merge.png](https://bitbucket.org/repo/BLzrb6/images/2641703444-edit-merge.png)

+ As Kaitlyn mentioned, once everyone is in agreement, then the pull request can be merged. On the pull request, look on the bottom right and you will see the merge button. 

> ![merge-pill-request.png](https://bitbucket.org/repo/BLzrb6/images/3580769461-merge-pill-request.png)

+ Once you are ready, click the merge button. However, If you no longer need the specific branch in the repository your are merging, then select the delete source branch after merging, and then click merge. 

+ That's it! The basics are simple. 

>![Branch-Merge.png](https://bitbucket.org/repo/BLzrb6/images/2776357354-Branch-Merge.png)

## **Stashing** ## 

+ Record your current state working directory and index, 

+ and also allowing you to go back to a clean working state.

> ![stashing.png](https://bitbucket.org/repo/BLzrb6/images/1328139064-stashing.png)

## **Git Fetch Command** ##
+ Brings our local copy of the remote repository up to date.  

> ![fetch.png](https://bitbucket.org/repo/BLzrb6/images/1712717169-fetch.png)  

## **Git Merge Command** ##

+ Go to the `Commands` Sub menu on top, and click on `Pull`  

> ![gitmenu.png](https://bitbucket.org/repo/BLzrb6/images/1593056188-gitmenu.png)  

+ Brings the changes in the remote repository to where we keep our own code. (Normally, `git pull` does this by doing 
 `git fetch` first to bring the local copy of the remote repository up to date, and then it merges the changes into
our own code repository with `git merge`.)  

> ![merge.png](https://bitbucket.org/repo/BLzrb6/images/3350597298-merge.png)  

## **Git Rebase Command** ##

+ Similar to `git merge`, creates a new commit, but also removes previous multiple branches. This leaves only a 
linear history of commits, making work more organized. (Partial history of commits is lost this way. Therefore, 
there might be complications when wanting to backtrack to previous versions.)  

> ![rebase.png](https://bitbucket.org/repo/BLzrb6/images/2512764082-rebase.png)

## **Git Reference Tools** ##

+ These reference tools will help you when using the command prompt. This Git Cheat Sheet can be downloaded at https://git-scm.com/docs. 

> ![Git-cmps-1.png](https://bitbucket.org/repo/BLzrb6/images/25388787-Git-cmps-1.png)
> ![Git-CMPS-2.png](https://bitbucket.org/repo/BLzrb6/images/1991835957-Git-CMPS-2.png)
> ![Git-CMPS-3.png](https://bitbucket.org/repo/BLzrb6/images/1230900931-Git-CMPS-3.png)
> ![Git-cmps-4.png](https://bitbucket.org/repo/BLzrb6/images/2361390467-Git-cmps-4.png)

+ There is another tool to assist you, which is an interactive NDP software located at http://ndpsoftware.com/git-cheatsheet.html#loc=workspace;

+ If anyone has any problems or bugs you can go to a reference website at https://git-scm.com/docs. This will give you documentation about all aspects of Git.


## **Running Into Problems** 

+ Undoing Merges/ Reversing Commits: There are a multiply ways to reverse your repository depending on what needs to be undone. One area to look at is undoing a merge. 

+ Git can in fact undo an entire merge. The most common command for this action is the Git revert command. This will allow you revert a single commit, or you can also use it to revert merge commits.

+ You could also run into the problem of needing to revert the revert. If you need to remerge something again, then Git will see your past history, which will prevent you from reverting your merge. Basically, you will have to actually revert the revert of the merge itself.

+ Here is a flowchart resource to help out with some possible messes you may come across while committing your work

>![flowchart.png](https://bitbucket.org/repo/BLzrb6/images/2346032706-flowchart.png)